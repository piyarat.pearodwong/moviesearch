//
//  SearchMovieInteractor.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

protocol SearchMovieInteractorInterface {
    func getSearch(request: SearchMovie.GetSearchData.Request)
    func sendSearchData(request: SearchMovie.SendSearchData.Request)
    //var model: Entity? { get }
    var searchData: String? { get }
}

class SearchMovieInteractor: SearchMovieInteractorInterface {
    var presenter: SearchMoviePresenterInterface!
    var worker: SearchMovieWorker?
    var searchData: String?
    var searchList: [String]?
    
    // MARK: - Business logic
    
    func getSearch(request: SearchMovie.GetSearchData.Request) {
        worker?.getSearch() { [weak self] coreDataResponse in switch coreDataResponse {
        case .success(let results):
            self?.searchList = results
            let response = SearchMovie.GetSearchData.Response(searchItems: self?.searchList ?? [])
            self?.presenter.presentSearchData(response: response)
        case .failure(let error):
            print(error)
        }
        }
    }
    
    func sendSearchData(request: SearchMovie.SendSearchData.Request){
        if request.searchLabel != "" {
            searchData = request.searchLabel
            let response = SearchMovie.SendSearchData.Response()
            presenter.presentSearchToSend(response: response)
        }
    }
}
