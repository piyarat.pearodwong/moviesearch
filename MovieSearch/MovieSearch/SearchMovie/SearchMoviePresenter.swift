//
//  SearchMoviePresenter.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchMoviePresenterInterface {
  func presentSearchData(response: SearchMovie.GetSearchData.Response)
    func presentSearchToSend(response: SearchMovie.SendSearchData.Response)
}

class SearchMoviePresenter: SearchMoviePresenterInterface {
  weak var viewController: SearchMovieViewControllerInterface!

  // MARK: - Presentation logic

  func presentSearchData(response: SearchMovie.GetSearchData.Response) {
      let searchItems = response.searchItems
      var searchList: [String] = []
      for searchItem in searchItems {
          let searchLabel = searchItem
          searchList.append(searchLabel)
      }
    

    let viewModel = SearchMovie.GetSearchData.ViewModel(searchModel: searchList)
    viewController.displaySearch(viewModel: viewModel)
  }
    
    func presentSearchToSend(response: SearchMovie.SendSearchData.Response){
        let viewModel = SearchMovie.SendSearchData.ViewModel()
        viewController.displayMovieList(viewModel: viewModel)
        
    }
}
