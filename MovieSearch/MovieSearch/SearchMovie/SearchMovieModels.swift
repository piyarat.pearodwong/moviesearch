//
//  SearchMovieModels.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct SearchMovie {
    /// This structure represents a use case
    struct GetSearchData {
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
            let searchItems: [String]
        }
        /// Data struct sent to ViewController
        struct ViewModel {
            var searchModel: [String]
        }
    }
    
    struct SendSearchData {
        struct Request {
            let searchLabel: String
        }
        struct Response {
            
        }
        struct ViewModel {
            
        }
    }
    
    
    
}
