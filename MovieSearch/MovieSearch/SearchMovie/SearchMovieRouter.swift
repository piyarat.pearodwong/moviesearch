//
//  SearchMovieRouter.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchMovieRouterInterface {
    func navigateToMovieList()
}

class SearchMovieRouter: SearchMovieRouterInterface {
    weak var viewController: SearchMovieViewController!
    
    // MARK: - Navigation
    
    func navigateToMovieList() {
        // NOTE: Teach the router how to navigate to another scene. Some examples follow:
        
        // 1. Trigger a storyboard segue
        viewController.performSegue(withIdentifier: "searchToList", sender: nil)
        
        // 2. Present another view controller programmatically
        // viewController.presentViewController(someWhereViewController, animated: true, completion: nil)
        
        // 3. Ask the navigation controller to push another view controller onto the stack
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
        
        // 4. Present a view controller from a different storyboard
        // let storyboard = UIStoryboard(name: "OtherThanMain", bundle: nil)
        // let someWhereViewController = storyboard.instantiateInitialViewController() as! SomeWhereViewController
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
    }
    
    // MARK: - Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        if segue.identifier == "searchToList" {
            passDataToMovieListScene(segue: segue)
        }
    }
    
    func passDataToMovieListScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        //print(viewController.interactor.searchData)
        let movieListViewController = segue.destination as! MovieListViewController
        movieListViewController.interactor.searchData = viewController.interactor.searchData
    }
}
