//
//  SearchMovieWorker.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchMovieStoreProtocol {
  func getSearch(_ completion: @escaping (Result<[String]>) -> Void)
}

class SearchMovieWorker {

  var store: SearchMovieStoreProtocol

  init(store: SearchMovieStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

  func getSearch(_ completion: @escaping (Result<[String]>) -> Void) {
    // NOTE: Do the work
    store.getSearch {
      // The worker may perform some small business logic before returning the result to the Interactor
      completion($0)
    }
  }
}
