//
//  SearchMovieViewController.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchMovieViewControllerInterface: class {
    func displaySearch(viewModel: SearchMovie.GetSearchData.ViewModel)
    func displayMovieList(viewModel: SearchMovie.SendSearchData.ViewModel)
}

class SearchMovieViewController: UITableViewController, UISearchBarDelegate, SearchMovieViewControllerInterface {
    var interactor: SearchMovieInteractorInterface!
    var router: SearchMovieRouter!
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
        
    }
    
    
    // MARK: - Configuration
    
    private func configure(viewController: SearchMovieViewController) {
        let router = SearchMovieRouter()
        router.viewController = viewController
        
        let presenter = SearchMoviePresenter()
        presenter.viewController = viewController
        
        let interactor = SearchMovieInteractor()
        interactor.presenter = presenter
        interactor.worker = SearchMovieWorker(store: SearchMovieStore())
        
        viewController.interactor = interactor
        viewController.router = router
        
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white}
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getSeachData()
    }
    
    // MARK: - Event handling
    
    func getSeachData() {
        // NOTE: Ask the Interactor to do some work
        
        let request = SearchMovie.GetSearchData.Request()
        interactor.getSearch(request: request)
    }
    
    // MARK: - Display logic
    
    var searchModels: [String] = []
    
    func displaySearch(viewModel: SearchMovie.GetSearchData.ViewModel) {
        
        searchModels = viewModel.searchModel
        tableView.reloadData()
    }
    
    func displayMovieList(viewModel: SearchMovie.SendSearchData.ViewModel){
        router.navigateToMovieList()
    }
    
    // MARK: - Router
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
    
    
    //MARK: - Search Methods
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchString = searchBar.text{
            if searchString != "" {
                let request = SearchMovie.SendSearchData.Request(searchLabel: searchString)
                interactor.sendSearchData(request: request)
                DispatchQueue.main.async {
                    self.clearSearchBar(searchBar)
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.showsCancelButton = true
        if searchBar.text?.count == 0 {
            DispatchQueue.main.async {
                self.clearSearchBar(searchBar)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        clearSearchBar(searchBar)
    }
    
    func clearSearchBar(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        
    }
    
    
    //MARK: - Tableview Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
        let searchData = searchModels[indexPath.row]
        cell.textLabel?.text = searchData
        
        return cell
    }
    
    //MARK: - TableView Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let request = SearchMovie.SendSearchData.Request(searchLabel: searchModels[indexPath.row])
        interactor.sendSearchData(request: request)
        
        
        //performSegue(withIdentifier: "searchToList", sender: self)
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
