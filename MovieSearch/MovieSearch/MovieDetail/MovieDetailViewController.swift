//
//  MovieDetailViewController.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import Kingfisher

protocol MovieDetailViewControllerInterface: class {
    func displayMovieDetail(viewModel: MovieDetail.SetupData.ViewModel)
    func displayFavoriteButtonDetail(viewModel: MovieDetail.SetFavoriteStatus.ViewModel)
}

class MovieDetailViewController: UIViewController, MovieDetailViewControllerInterface {
    var interactor: MovieDetailInteractorInterface!
    var router: MovieDetailRouter!
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var averageNumber: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var buttonLabel: UIButton!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: MovieDetailViewController) {
        let router = MovieDetailRouter()
        router.viewController = viewController
        
        let presenter = MovieDetailPresenter()
        presenter.viewController = viewController
        
        let interactor = MovieDetailInteractor()
        interactor.presenter = presenter
        interactor.worker = MovieDetailWorker(store: MovieDetailStore())
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMovieDetail()
        checkFavoriteData()
    }
    
    // MARK: - Event handling
    
    func getMovieDetail() {
        // NOTE: Ask the Interactor to do some work
        let request = MovieDetail.SetupData.Request()
        interactor.setupData(request: request)
    }
    func checkFavoriteData() {
        // NOTE: Ask the Interactor to do some work
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.checkFavorite(request: request)
    }
    
    // MARK: - Display logic
    
    func displayMovieDetail(viewModel: MovieDetail.SetupData.ViewModel) {
        movieName.text = viewModel.title
        averageNumber.text = viewModel.average
        overview.text = viewModel.overview
        
        if viewModel.poster_path != ""
        {
            let url = URL(string: "https://image.tmdb.org/t/p/w92/\(viewModel.poster_path)")
            posterImage.kf.setImage(with: url)
            
        }else{
            posterImage.image = UIImage(systemName: "photo.artframe")
        }
        
    }
    
    func displayFavoriteButtonDetail(viewModel: MovieDetail.SetFavoriteStatus.ViewModel){
        buttonLabel.setTitle(viewModel.label, for: .normal)
    }
    
    // MARK: - Router
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
    
    @IBAction func unwindToMovieDetailViewController(from segue: UIStoryboardSegue) {
        print("unwind...")
        router.passDataToNextScene(segue: segue)
    }
    
    @IBAction func favoritePressed(_ sender: UIButton) {
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.setFavorite(request: request)
    }
    
    @IBAction func backToSearchPressed(_ sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
