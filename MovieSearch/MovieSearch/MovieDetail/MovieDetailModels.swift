//
//  MovieDetailModels.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct MovieDetail {
    /// This structure represents a use case
    struct SetupData {
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
            let movie: Movie
        }
        /// Data struct sent to ViewController
        struct ViewModel {
            let title : String
            let poster_path : String
            let average : String
            let overview : String
        }
    }
    struct SetFavoriteStatus{
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
            let label: String
        }
        /// Data struct sent to ViewController
        struct ViewModel {
            let label : String
        }
    }
}
