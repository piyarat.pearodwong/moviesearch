//
//  MovieListTableViewCell.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//

import UIKit
import Kingfisher

class MovieListTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var movieOverview: UILabel!
    
    func setMovie(movie: MovieList.GetMovieList.ViewModel.MovieViewModel){
        
        if movie.poster_path != ""
        {
            let url = URL(string: "https://image.tmdb.org/t/p/w92/\(movie.poster_path)")
            posterImage.kf.setImage(with: url)
            
        }else{
            posterImage.image = UIImage(systemName: "photo.artframe")
        }
        movieTitle.text = movie.title
        releaseDate.text = movie.release_date
        movieOverview.text = movie.overview
        
    }
    func setMovie(movie: FavoriteMovie.GetFavoriteList.ViewModel.MovieViewModel){
        
        if movie.poster_path != ""
        {
            let url = URL(string: "https://image.tmdb.org/t/p/w92/\(movie.poster_path)")
            posterImage.kf.setImage(with: url)
            
        }else{
            posterImage.image = UIImage(systemName: "photo.artframe")
        }
        movieTitle.text = movie.title
        releaseDate.text = movie.release_date
        movieOverview.text = movie.overview
        
    }

}
