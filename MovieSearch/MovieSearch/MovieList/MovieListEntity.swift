//
//  MovieListEntity.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

/*

 The Entity class is the business object.
 The Result enumeration is the domain model.

 1. Rename this file to the desired name of the model.
 2. Rename the Entity class with the desired name of the model.
 3. Move this file to the Models folder.
 4. If the project already has a declaration of Result enumeration, you may remove the declaration from this file.
 5. Remove the following comments from this file.

 */
enum Result<T> {
  case success(T)
  case failure(Error)
}

//
// The entity or business object
//
struct MovieResult : Codable {
    let page: Int
    let total_pages: Int
    let results : [Movie]
}

struct Movie : Codable {
    let id: Int
    let poster_path: String?
    let title: String
    let release_date: String?
    let overview: String?
    let vote_average: Double?
}
