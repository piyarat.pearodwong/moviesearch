//
//  MovieListPresenter.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieListPresenterInterface {
    func presentMovies(response: MovieList.GetMovieList.Response)
    func presentSelectedIndex(response: MovieList.SetSelectedIndex.Response)
}

class MovieListPresenter: MovieListPresenterInterface {
    weak var viewController: MovieListViewControllerInterface!
    
    // MARK: - Presentation logic
    
    func presentMovies(response: MovieList.GetMovieList.Response) {
        let movies = response.movieList
        var movieViewModels: [MovieList.GetMovieList.ViewModel.MovieViewModel] = []
        for movie in movies {
            let id = movie.id
            let title = movie.title
            let poster_path = movie.poster_path ?? ""
            let overview = movie.overview ?? ""
            let release_date = movie.release_date ?? ""
            
            var average : String = ""
            if let vote_average = movie.vote_average{
                average = String(vote_average)
            }
            let movieViewModel = MovieList.GetMovieList.ViewModel.MovieViewModel(id: id, title: title, poster_path: poster_path, overview: overview, release_date: release_date, average: average)
            movieViewModels.append(movieViewModel)
        }
        
        let viewModel = MovieList.GetMovieList.ViewModel(movieViewModels: movieViewModels)
        viewController.displayMovies(viewModel: viewModel)
    }
    
    func presentSelectedIndex(response: MovieList.SetSelectedIndex.Response) {
        let viewModel = MovieList.SetSelectedIndex.ViewModel()
        viewController.displaySelectedIndex(viewModel: viewModel)
    }
}
