//
//  MovieListWorker.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieListStoreProtocol {
  func getMovieList(search: String,page: Int,_ completion: @escaping (Result<MovieResult>) -> Void)
}

class MovieListWorker {

  var store: MovieListStoreProtocol

  init(store: MovieListStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

  func getMovieList(search: String,page: Int,_ completion: @escaping (Result<MovieResult>) -> Void) {
    // NOTE: Do the work
    store.getMovieList(search: search, page: page) {
      result in completion(result)
    }
  }
}
