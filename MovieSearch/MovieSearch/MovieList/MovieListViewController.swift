//
//  MovieListViewController.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieListViewControllerInterface: class {
    func displayMovies(viewModel: MovieList.GetMovieList.ViewModel)
    func displaySelectedIndex(viewModel: MovieList.SetSelectedIndex.ViewModel)
}

class MovieListViewController: UITableViewController, MovieListViewControllerInterface {
    var interactor: MovieListInteractorInterface!
    var router: MovieListRouter!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: MovieListViewController) {
        let router = MovieListRouter()
        router.viewController = viewController
        
        let presenter = MovieListPresenter()
        presenter.viewController = viewController
        
        let interactor = MovieListInteractor()
        interactor.presenter = presenter
        interactor.worker = MovieListWorker(store: MovieListStore())
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - Event handling
    
    func getMovieList() {
        // NOTE: Ask the Interactor to do some work
        let request = MovieList.GetMovieList.Request()
        interactor.getMovieList(request: request)
    }
    
    // MARK: - Display logic
    
    var movieViewModels: [MovieList.GetMovieList.ViewModel.MovieViewModel] = []
    
    func displayMovies(viewModel: MovieList.GetMovieList.ViewModel) {
        // NOTE: Display the result from the Presenter
        movieViewModels = viewModel.movieViewModels
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.removeLoadingScreen()
            DispatchQueue.main.async {
                self.isLoading = false
            }
        }
    }
    
    func displaySelectedIndex(viewModel: MovieList.SetSelectedIndex.ViewModel) {
        router.navigateToMovieDetail()
    }
    
    // MARK: - Router
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
    
    @IBAction func unwindToMovieListViewController(from segue: UIStoryboardSegue) {
        print("unwind...")
        //router.passDataToNextScene(segue: segue)
    }
    //MARK: - TableView Datasoucre Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //put data in a cell of tabelview
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieListCell", for: indexPath) as! MovieListTableViewCell
        
        let item = movieViewModels[indexPath.row]
        cell.setMovie(movie: item)
        
        return cell
    }
    
    //MARK: - Tableview Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = MovieList.SetSelectedIndex.Request(index: indexPath.row)
        interactor.setSelectedIndex(request: request)
    }
    
    
    //MARK: - ScrollView Methods
    
    var isLoading = false
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height && !isLoading {
            loadMoreData()
        }
    }
    
    func loadMoreData(){
        
        if !isLoading {
            isLoading = true
            DispatchQueue.global().async {
                // Fake background loading task for 2 seconds
                sleep(1)
                // Download more data here
                DispatchQueue.main.async {
                    if self.interactor.searchPage <= self.interactor.totalPage{
                        self.setLoadingScreen()
                        self.getMovieList()
                    }
                }
            }
        }
    }
    
    
    //MARK: - Indicator Methods
    
    // View which contains the loading text and the spinner
    let loadingView = UIView()
    
    // Spinner shown during load the TableView
    let spinner = UIActivityIndicatorView()
    
    // Text shown during load the TableView
    let loadingLabel = UILabel()
    
    // Set the activity indicator into the main view
    private func setLoadingScreen() {
        // Sets the view which contains the loading text and the spinner
        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 120
        let height: CGFloat = 120
        let x = (tableView.frame.width / 2) - (width / 2)
        let y = (tableView.frame.height / 2) - (height / 2)
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)
        loadingView.backgroundColor = UIColor(red: 0.27, green: 0.27, blue: 0.27, alpha: 1.00)
        loadingView.layer.cornerRadius = 15
        
        // Sets loading text
        loadingLabel.textColor = .white
        loadingLabel.textAlignment = .center
        loadingLabel.text = "Loading..."
        loadingLabel.frame = CGRect(x: 0, y: (loadingView.frame.height/2 ) , width: 140, height: 30)
        loadingLabel.center.x = loadingView.frame.width/2
        
        // Sets spinner
        spinner.style = .white
        spinner.frame = CGRect(x: 0, y: (loadingView.frame.height/4) , width: 30, height: 30)
        spinner.center.x = loadingView.frame.width/2
        //spinner.center.y = loadingView.frame.height/2
        
        //Set Animation
        spinner.startAnimating()
        spinner.isHidden = false
        loadingLabel.isHidden = false
        loadingView.isHidden = false
        
        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        loadingView.addSubview(loadingLabel)
        
        tableView.addSubview(loadingView)
        tableView.superview?.addSubview(loadingView)
    }
    
    // Remove the activity indicator from the main view
    private func removeLoadingScreen() {
        
        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
        loadingLabel.isHidden = true
        loadingView.isHidden = true
        
        
    }
    
}
