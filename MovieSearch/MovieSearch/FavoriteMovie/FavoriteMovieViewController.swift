//
//  FavoriteMovieViewController.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol FavoriteMovieViewControllerInterface: class {
    func displayFavoriteMovies(viewModel: FavoriteMovie.GetFavoriteList.ViewModel)
    func displaySelectedIndex(viewModel: FavoriteMovie.SetSelectedIndex.ViewModel)
}

class FavoriteMovieViewController: UITableViewController, FavoriteMovieViewControllerInterface {
    var interactor: FavoriteMovieInteractorInterface!
    var router: FavoriteMovieRouter!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: FavoriteMovieViewController) {
        let router = FavoriteMovieRouter()
        router.viewController = viewController
        
        let presenter = FavoriteMoviePresenter()
        presenter.viewController = viewController
        
        let interactor = FavoriteMovieInteractor()
        interactor.presenter = presenter
        interactor.worker = FavoriteMovieWorker(store: FavoriteMovieStore())
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getFavoriteMovie()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFavoriteMovie()
    }
    
    // MARK: - Event handling
    
    func getFavoriteMovie() {
        let request = FavoriteMovie.GetFavoriteList.Request()
        interactor.getFavoriteList(request: request)
    }
    
    // MARK: - Display logic
    var movieViewModels: [FavoriteMovie.GetFavoriteList.ViewModel.MovieViewModel] = []
    func displayFavoriteMovies(viewModel: FavoriteMovie.GetFavoriteList.ViewModel) {
        // NOTE: Display the result from the Presenter
        movieViewModels = viewModel.movieViewModels
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func displaySelectedIndex(viewModel: FavoriteMovie.SetSelectedIndex.ViewModel) {
        router.navigateToMovieDetail()
    }
    
    // MARK: - Router
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
    
    @IBAction func unwindToFavoriteMovieViewController(from segue: UIStoryboardSegue) {
        print("unwind...")
        router.passDataToNextScene(segue: segue)
    }
    
    
    //MARK: - Tableview Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteCell", for: indexPath) as! MovieListTableViewCell
        let item = movieViewModels[indexPath.row]
        cell.setMovie(movie: item)
        
        return cell
        
    }
    
    //MARK: - Tableview Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = FavoriteMovie.SetSelectedIndex.Request(index: indexPath.row)
        interactor.setSelectedIndex(request: request)
    }
    
    
}
