//
//  FavoriteMovieStore.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/*
 
 The FavoriteMovieStore class implements the FavoriteMovieStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */

class FavoriteMovieStore: FavoriteMovieStoreProtocol {

    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    func getFavoriteList(_ completion: @escaping (Result<[Movie]>) -> Void) {
        let request:NSFetchRequest<FavoriteList> = FavoriteList.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        do{
            let results = try context.fetch(request)
            var movies = [Movie]()
            for result in results {
                let id = Int(result.id)
                let title = result.title ?? ""
                let release_date = result.release_date ?? ""
                let poster_path = result.poster_path ?? ""
                let average = result.vote_average
                let overview = result.overview ?? ""
                let movie = Movie(id: id, poster_path: poster_path, title: title, release_date: release_date, overview: overview, vote_average: average)
                movies.append(movie)
            }
            completion(.success(movies))
        }catch{
            print("Error fetching data from context \(error)")
        }
    }
}
