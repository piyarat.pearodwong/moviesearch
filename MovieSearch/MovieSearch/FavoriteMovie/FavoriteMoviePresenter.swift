//
//  FavoriteMoviePresenter.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol FavoriteMoviePresenterInterface {
    func presentMovies(response: FavoriteMovie.GetFavoriteList.Response)
    func presentSelectedIndex(response: FavoriteMovie.SetSelectedIndex.Response)
}

class FavoriteMoviePresenter: FavoriteMoviePresenterInterface {
    weak var viewController: FavoriteMovieViewControllerInterface!
    
    // MARK: - Presentation logic
    
    func presentMovies(response: FavoriteMovie.GetFavoriteList.Response) {
        let movies = response.movieList
        var movieViewModels: [FavoriteMovie.GetFavoriteList.ViewModel.MovieViewModel] = []
        for movie in movies {
            let id = movie.id
            let title = movie.title
            let poster_path = movie.poster_path ?? ""
            let overview = movie.overview ?? ""
            let release_date = movie.release_date ?? ""
            
            var average : String = ""
            if let vote_average = movie.vote_average{
                average = String(vote_average)
            }
            let movieViewModel = FavoriteMovie.GetFavoriteList.ViewModel.MovieViewModel(id: id, title: title, poster_path: poster_path, overview: overview, release_date: release_date, average: average)
            movieViewModels.append(movieViewModel)
        }
        
        let viewModel = FavoriteMovie.GetFavoriteList.ViewModel(movieViewModels: movieViewModels)
        viewController.displayFavoriteMovies(viewModel: viewModel)
    }
    
    func presentSelectedIndex(response: FavoriteMovie.SetSelectedIndex.Response) {
        let viewModel = FavoriteMovie.SetSelectedIndex.ViewModel()
        viewController.displaySelectedIndex(viewModel: viewModel)
    }
}
