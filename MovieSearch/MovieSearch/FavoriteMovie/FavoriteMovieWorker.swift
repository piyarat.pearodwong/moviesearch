//
//  FavoriteMovieWorker.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol FavoriteMovieStoreProtocol {
  func getFavoriteList(_ completion: @escaping (Result<[Movie]>) -> Void)
}

class FavoriteMovieWorker {

  var store: FavoriteMovieStoreProtocol

  init(store: FavoriteMovieStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

  func getFavoriteList(_ completion: @escaping (Result<[Movie]>) -> Void) {
    // NOTE: Do the work
    store.getFavoriteList{
      // The worker may perform some small business logic before returning the result to the Interactor
      completion($0)
    }
  }
}
