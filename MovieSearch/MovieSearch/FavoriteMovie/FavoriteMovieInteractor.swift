//
//  FavoriteMovieInteractor.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol FavoriteMovieInteractorInterface {
    func getFavoriteList(request: FavoriteMovie.GetFavoriteList.Request)
    func setSelectedIndex(request: FavoriteMovie.SetSelectedIndex.Request)
    
    var selectedMovie: Movie? { get }
}

class FavoriteMovieInteractor: FavoriteMovieInteractorInterface {
    var presenter: FavoriteMoviePresenterInterface!
    var worker: FavoriteMovieWorker?
    var movies: [Movie] = []
    var selectedMovie: Movie?
    
    // MARK: - Business logic
    
    func getFavoriteList(request: FavoriteMovie.GetFavoriteList.Request) {
        worker?.getFavoriteList() { [weak self] coreDataResponse in switch coreDataResponse {
        case .success(let results):
            self?.movies = results
            let response = FavoriteMovie.GetFavoriteList.Response(movieList: self?.movies ?? [])
            self?.presenter.presentMovies(response: response)
        case .failure(let error):
            print(error)
        }
        }
    }
    
    func setSelectedIndex(request: FavoriteMovie.SetSelectedIndex.Request) {
        selectedMovie = movies[request.index]
        let response = FavoriteMovie.SetSelectedIndex.Response()
        presenter.presentSelectedIndex(response: response)
    }
    
    
}
