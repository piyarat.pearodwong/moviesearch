//
//  MovieDetailInteractorTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/30/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class MovieDetailInteractorTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var interactor: MovieDetailInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMovieDetailInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMovieDetailInteractor() {
        interactor = MovieDetailInteractor()
    }
    
    // MARK: - Test doubles
    class MovieDetailWorkerTest: MovieDetailWorker {
        var checkDataCalled: Bool = false
        var checkDataResult: Result<String>?
        override func checkData(id: Int, _ completion: @escaping (Result<String>) -> Void) {
            checkDataCalled = true
            var results = [Movie]()
            if id == 1 {
                results = [
                    Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
                ]
            }else {
                results = []
            }
            if results.count != 0 {
                checkDataResult = Result.success("Unfavorite")
                completion(.success("Unfavorite"))
            }else {
                checkDataResult = Result.success("Favorite")
                completion(.success("Favorite"))
            }
        }
        
        var saveDataCalled: Bool = false
        var saveDataResult: Result<String>?
        override func saveData(movie: Movie, _ completion: @escaping (Result<String>) -> Void) {
            saveDataCalled = true
            saveDataResult = Result.success("Unfavorite")
            completion(.success("Unfavorite"))
        }
        
        var deleteDataCalled: Bool = false
        var deleteDataResult: Result<String>?
        override func deleteData(id: Int, _ completion: @escaping (Result<String>) -> Void) {
            deleteDataCalled = true
            var results = [Movie]()
            if id == 1 {
                results = [
                    Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
                ]
            }else {
                results = []
            }
            if results.count != 0 {
                deleteDataResult = Result.success("Favorite")
                completion(.success("Favorite"))
            }
        }
    }
    
    class OutputTest: MovieDetailPresenterInterface {
        var presentMovieDetailCalled: Bool = false
        var presentMovieDetailResponse: MovieDetail.SetupData.Response?
        func presentMovieDetail(response: MovieDetail.SetupData.Response) {
            presentMovieDetailCalled = true
            presentMovieDetailResponse = response
        }
        
        var presentFavoriteStatusCalled: Bool = false
        var presentFavoriteStatusResponse: MovieDetail.SetFavoriteStatus.Response?
        func presentFavoriteStatus(response: MovieDetail.SetFavoriteStatus.Response) {
            presentFavoriteStatusCalled = true
            presentFavoriteStatusResponse = response
        }
    }
    
    
    // MARK: - Tests
    
    func testSetupData() {
        // Given
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = MovieDetail.SetupData.Request()
        interactor.movie = Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
        
        // When
        interactor.setupData(request: request)
        
        // Then
        XCTAssert(outputTest.presentMovieDetailCalled)
        if let response = outputTest.presentMovieDetailResponse{
            XCTAssertEqual(response.movie.id, 1)
        }
    }
    
    func testCheckFavoriteCaseHaveData() {
        // Given
        let workerTest: MovieDetailWorkerTest = MovieDetailWorkerTest(store: MovieDetailStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.movie = Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
        
        // When
        interactor.checkFavorite(request: request)
        
        // Then
        XCTAssert(workerTest.checkDataCalled)
        if let result = workerTest.checkDataResult{
            switch result {
            case .success(let data):
                XCTAssertEqual(data, "Unfavorite")
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentFavoriteStatusCalled)
        if let response = outputTest.presentFavoriteStatusResponse{
            XCTAssertEqual(response.label, "Unfavorite")
        }
    }
    
    func testCheckFavoriteCaseNoData() {
        // Given
        let workerTest: MovieDetailWorkerTest = MovieDetailWorkerTest(store: MovieDetailStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.movie = Movie(id: 2, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
        
        // When
        interactor.checkFavorite(request: request)
        
        // Then
        XCTAssert(workerTest.checkDataCalled)
        if let result = workerTest.checkDataResult{
            switch result {
            case .success(let data):
                XCTAssertEqual(data, "Favorite")
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentFavoriteStatusCalled)
        if let response = outputTest.presentFavoriteStatusResponse{
            XCTAssertEqual(response.label, "Favorite")
        }
    }
    
    func testSetFavoriteCaseSaveData() {
        // Given
        let workerTest: MovieDetailWorkerTest = MovieDetailWorkerTest(store: MovieDetailStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.movie = Movie(id: 1, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
        interactor.label = "Favorite"
        
        // When
        interactor.setFavorite(request: request)
        
        // Then
        XCTAssert(workerTest.saveDataCalled)
        if let result = workerTest.saveDataResult{
            switch result {
            case .success(let data):
                XCTAssertEqual(data, "Unfavorite")
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentFavoriteStatusCalled)
        if let response = outputTest.presentFavoriteStatusResponse{
            XCTAssertEqual(response.label, "Unfavorite")
        }
    }
    
    func testSetFavoriteCaseDeleteData() {
        // Given
        let workerTest: MovieDetailWorkerTest = MovieDetailWorkerTest(store: MovieDetailStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = MovieDetail.SetFavoriteStatus.Request()
        interactor.movie = Movie(id: 1, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
        interactor.label = "Unfavorite"
        
        // When
        interactor.setFavorite(request: request)
        
        // Then
        XCTAssert(workerTest.deleteDataCalled)
        if let result = workerTest.deleteDataResult{
            switch result {
            case .success(let data):
                XCTAssertEqual(data, "Favorite")
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentFavoriteStatusCalled)
        if let response = outputTest.presentFavoriteStatusResponse{
            XCTAssertEqual(response.label, "Favorite")
        }
    }
    
    
    
}
