//
//  MovieDetailPresenterTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/30/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class MovieDetailPresenterTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var presenter: MovieDetailPresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMovieDetailPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMovieDetailPresenter() {
        presenter = MovieDetailPresenter()
    }
    
    // MARK: - Test doubles
    
    class OutputTest: MovieDetailViewControllerInterface{
        var displayMovieDetailCalled: Bool = false
        var displayMovieDetailViewModel: MovieDetail.SetupData.ViewModel?
        func displayMovieDetail(viewModel: MovieDetail.SetupData.ViewModel) {
            displayMovieDetailCalled = true
            displayMovieDetailViewModel = viewModel
        }
        
        var displayFavoriteButtonDetailCalled: Bool = false
        var displayFavoriteButtonDetailViewModel: MovieDetail.SetFavoriteStatus.ViewModel?
        func displayFavoriteButtonDetail(viewModel: MovieDetail.SetFavoriteStatus.ViewModel) {
            displayFavoriteButtonDetailCalled = true
            displayFavoriteButtonDetailViewModel = viewModel
        }
    }
    // MARK: - Tests
    
    func testPresentMovieDetail() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let movieModel = Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
        let response = MovieDetail.SetupData.Response(movie: movieModel)
        
        // When
        presenter.presentMovieDetail(response: response)
        
        // Then
        XCTAssert(outputTest.displayMovieDetailCalled)
        if let result = outputTest.displayMovieDetailViewModel{
            XCTAssertEqual(result.title, "Frozen1")
            XCTAssertEqual(result.average, "10.0")
        }
    }
    
    func testPresentFavoriteStatus() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let label = "Favorite"
        let response = MovieDetail.SetFavoriteStatus.Response(label: label)
        
        // When
        presenter.presentFavoriteStatus(response: response)
        
        // Then
        XCTAssert(outputTest.displayFavoriteButtonDetailCalled)
        if let result = outputTest.displayFavoriteButtonDetailViewModel{
            XCTAssertEqual(result.label, "Favorite")
        }
    }
}
