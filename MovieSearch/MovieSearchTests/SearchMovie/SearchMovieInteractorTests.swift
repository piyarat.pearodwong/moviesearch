//
//  SearchMovieInteractorTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/28/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class SearchMovieInteractorTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var interactor: SearchMovieInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupSearchMovieInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupSearchMovieInteractor() {
        interactor = SearchMovieInteractor()
    }
    
    // MARK: - Test doubles
    class SearchMovieWorkerTest: SearchMovieWorker {
        var getSearchCalled: Bool = false
        var getSearchResult: Result<[String]>?
        
        override func getSearch(_ completion: @escaping (Result<[String]>) -> Void) {
            getSearchCalled = true
            let searchList: [String] = ["Frozen","Harry","Disney"]
            getSearchResult = Result.success(searchList)
            completion(Result.success(searchList))
        }
    }
    
    class OutputTest : SearchMoviePresenterInterface {
        var presentSearchDataCalled: Bool = false
        var presentSearchDataResponse: SearchMovie.GetSearchData.Response?
        
        func presentSearchData(response: SearchMovie.GetSearchData.Response) {
            presentSearchDataCalled = true
            presentSearchDataResponse = response
        }
        
        var presentSearchToSendCalled: Bool = false
        var presentSearchToSendResponse: SearchMovie.SendSearchData.Response?
        
        func presentSearchToSend(response: SearchMovie.SendSearchData.Response) {
            presentSearchToSendCalled = true
            presentSearchToSendResponse = response
        }
    }
    
    
    
    // MARK: - Tests
    
    func testGetSearch() {
        // Given
        let workerTest: SearchMovieWorkerTest = SearchMovieWorkerTest(store: SearchMovieStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = SearchMovie.GetSearchData.Request()
        
        // When
        interactor.getSearch(request: request)
        
        // Then
        XCTAssert(workerTest.getSearchCalled)
        if let result = workerTest.getSearchResult {
            switch result {
            case .success(let data):
                XCTAssertEqual(data.count, 3)
            case .failure(_):
              XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentSearchDataCalled)
        if let response = outputTest.presentSearchDataResponse {
            XCTAssertEqual(response.searchItems.count, 3)
        }
        
    }
    func testSendSearchData() {
        // Given
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        let search = "Frozen"
        let request = SearchMovie.SendSearchData.Request(searchLabel: search)
        
        // When
        interactor.sendSearchData(request: request)
        
        // Then
        XCTAssert(outputTest.presentSearchToSendCalled)
        XCTAssertEqual(interactor.searchData, search)
    }
    
}


