//
//  SearchMoviePresenterTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/28/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class SearchMoviePresenterTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var presenter: SearchMoviePresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupSearchMoviePresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupSearchMoviePresenter() {
        presenter = SearchMoviePresenter()
    }
    
    // MARK: - Test doubles
    class OutputTest: SearchMovieViewControllerInterface{
        var displaySearchCalled: Bool = false
        var displaySearchViewModel: SearchMovie.GetSearchData.ViewModel?
        func displaySearch(viewModel: SearchMovie.GetSearchData.ViewModel) {
            displaySearchCalled = true
            displaySearchViewModel = viewModel
        }
        
        var displayMovieListCalled:Bool = false
        var displayMovieListViewModel: SearchMovie.SendSearchData.ViewModel?
        func displayMovieList(viewModel: SearchMovie.SendSearchData.ViewModel) {
            displayMovieListCalled = true
            displayMovieListViewModel = viewModel
        }
    }
    
    
    // MARK: - Tests
    
    func testPresentSearchData() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let searchModel: [String] = ["Frozen","Harry","Disney"]
        let response = SearchMovie.GetSearchData.Response(searchItems: searchModel)
        
        // When
        presenter.presentSearchData(response: response)
        // Then
        XCTAssert(outputTest.displaySearchCalled)
        if let result = outputTest.displaySearchViewModel{
            XCTAssertEqual(result.searchModel.count, 3)
        }
        
    }
    
    func testPresentSearchToSend() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let response = SearchMovie.SendSearchData.Response()
        
        // When
        presenter.presentSearchToSend(response: response)
        // Then
        XCTAssert(outputTest.displayMovieListCalled)
    }
}
