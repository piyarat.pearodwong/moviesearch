//
//  MovieListInteractorTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/28/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class MovieListInteractorTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var interactor: MovieListInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMovieListInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMovieListInteractor() {
        interactor = MovieListInteractor()
    }
    
    // MARK: - Test doubles
    
    class MovieListWorkerTest: MovieListWorker {
        var getMovieListCalled: Bool = false
        var getMovieListResult: Result<MovieResult>?
        
        override func getMovieList(search: String, page: Int, _ completion: @escaping (Result<MovieResult>) -> Void) {
            getMovieListCalled = true
            let movies: [Movie] = [
            Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10),
            Movie(id: 2, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
            ]
            let result = MovieResult(page: 1, total_pages: 8, results: movies)
            let movieResult: Result<MovieResult> = Result.success(result)
            getMovieListResult = movieResult
            completion(movieResult)
        }
    }
    
    class OutputTest: MovieListPresenterInterface{
        var presentMoviesCalled: Bool = false
        var presentMoviesResponse: MovieList.GetMovieList.Response?
        func presentMovies(response: MovieList.GetMovieList.Response) {
            presentMoviesCalled = true
            presentMoviesResponse = response
        }
        
        var presentSelectedIndexCalled: Bool = false
        var presentSelectedIndexResponse: MovieList.SetSelectedIndex.Response?
        func presentSelectedIndex(response: MovieList.SetSelectedIndex.Response) {
            presentSelectedIndexCalled = true
            presentSelectedIndexResponse = response
        }
        
    }
    
    
    // MARK: - Tests
    
    func testGetMovieList() {
        // Given
        let workerTest: MovieListWorkerTest = MovieListWorkerTest(store: MovieListStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        interactor.searchData = "Frozen"
        let request = MovieList.GetMovieList.Request()
        
        // When
        interactor.getMovieList(request: request)
        
        // Then
        
        XCTAssert(workerTest.getMovieListCalled)
        print(workerTest.getMovieListResult)
        if let result = workerTest.getMovieListResult{
            switch result {
            case .success(let data):
                XCTAssertEqual(data.page, 1)
                XCTAssertEqual(data.results.count, 2)
                
                XCTAssertEqual(interactor.searchPage, 2)
                XCTAssertEqual(interactor.totalPage, 8)
                XCTAssertEqual(interactor.movies.count, 2)
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentMoviesCalled)
        if let response = outputTest.presentMoviesResponse{
            XCTAssertEqual(response.movieList.count, 2)
        }
        
    }
    
    func testSetSelectedIndex() {
        // Given
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let movie = Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
        interactor.movies = [movie]
        
        let request = MovieList.SetSelectedIndex.Request(index: 0)
        
        // When
        interactor.setSelectedIndex(request: request)
        // Then
        XCTAssert(outputTest.presentSelectedIndexCalled)
        XCTAssertEqual(interactor.selectedMovie?.id,movie.id)
        
    }
}
