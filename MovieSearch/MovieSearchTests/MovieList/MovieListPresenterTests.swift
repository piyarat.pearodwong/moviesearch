//
//  MovieListPresenterTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/28/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class MovieListPresenterTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var presenter: MovieListPresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMovieListPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMovieListPresenter() {
        presenter = MovieListPresenter()
    }
    
    // MARK: - Test doubles
    
    class OutputTest: MovieListViewControllerInterface{
        var displayMovieCalled: Bool = false
        var displayMovieViewModel: MovieList.GetMovieList.ViewModel?
        func displayMovies(viewModel: MovieList.GetMovieList.ViewModel) {
            displayMovieCalled = true
            displayMovieViewModel = viewModel
        }
        
        var displaySelectedIndexCalled: Bool = false
        var displaySelectedIndexViewModel: MovieList.SetSelectedIndex.ViewModel?
        func displaySelectedIndex(viewModel: MovieList.SetSelectedIndex.ViewModel) {
            displaySelectedIndexCalled = true
            displaySelectedIndexViewModel = viewModel
        }
    }
    // MARK: - Tests
    
    func testPresentMovies() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let moviesModel: [Movie] = [
            Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10),
            Movie(id: 2, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
        ]
        let response = MovieList.GetMovieList.Response(movieList: moviesModel)
        
        // When
        presenter.presentMovies(response: response)
        
        // Then
        XCTAssert(outputTest.displayMovieCalled)
        if let result = outputTest.displayMovieViewModel {
            XCTAssertEqual(result.movieViewModels.count, 2)
        }
        
        
        
    }
    func testPresentSelectedIndex() {
        // Given
        let outputTest = OutputTest()
        presenter.viewController = outputTest
        
        let response = MovieList.SetSelectedIndex.Response()
        
        // When
        presenter.presentSelectedIndex(response: response)
        
        // Then
        XCTAssert(outputTest.displaySelectedIndexCalled)
    }
    
}
